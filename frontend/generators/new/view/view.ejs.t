---
to: "src/router/views/<%= h.changeCase.kebab(name) %>.vue"
---
<%
  const fileName = h.changeCase.kebab(name)
  const importName = h.changeCase.pascal(fileName)
  const titleName = h.changeCase.title(name)
%><script>


export default {
  page: {
    title: '<%= titleName %>',
    meta: [{ name: 'description', content: 'The <%= titleName %> page.' }],
  },
  components: {  },
  data() {
    return {

    }
  }
}
</script>

<template>
  <fragment>
    <%= titleName %>
  </fragment>
</template>
<%

if (useStyles) { %>
<style lang="scss" module>
</style>
<% } %>
