import _ from 'lodash'

export const filterByValue = (objects, value) =>
  _.filter(
    objects,
    _.flow(
      _.values,
      _.partialRight(_.some, _.method('match', new RegExp(value, 'i')))
    )
  )
