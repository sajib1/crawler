import AdminDashboard from './admin-dashboard'

describe('@views/admin-dashboard', () => {
  it('is a valid view', () => {
    expect(AdminDashboard).toBeAViewComponent()
  })
})
