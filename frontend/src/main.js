import router from '@router'
import store from '@state/store'
import Default from '@router/layouts/base-layout.vue'
import Login from '@router/layouts/login-layout.vue'
import { Plugin } from 'vue-fragment'
import Vue from 'vue'
import App from './app.vue'
import VueBarcodeScanner from 'vue-barcode-scanner'
import VueToastr from 'vue-toastr'

window.Vue = Vue

Vue.use(VueToastr, {
  defaultTimeout: 3000,
  defaultProgressBar: true,
  defaultProgressBarValue: 0,
  defaultType: 'success',
  defaultPosition: 'toast-top-right',
  defaultCloseOnHover: true,
  defaultClassNames: ['animated', 'zoomInUp'],
})
// Globally register all `_base`-prefixed components
import '@components/_globals'
import '@assets/vendors/vendors.min.css'
import '@assets/sass/themes/vertical-modern-menu-template/materialize.scss'
import '@assets/sass/themes/vertical-modern-menu-template/style.scss'
import '@progress/kendo-theme-material/dist/all.css'
import '@assets/sass/custom/custom.scss'

// Don't warn about using the dev version of Vue in development.
Vue.config.productionTip = process.env.NODE_ENV === 'production'

Vue.component('default-layout', Default)
Vue.component('login-layout', Login)
Vue.use(Plugin)



let barcode_options = {
  sound: true, // default is false
  soundSrc: '/static/sound.wav', // default is blank
  sensitivity: 300, // default is 100
  requiredAttr: true, // default is false
  controlSequenceKeys: ['NumLock', 'Clear'], // default is null
  callbackAfterTimeout: true, // default is false
}

Vue.use(VueBarcodeScanner, barcode_options)

// If running inside Cypress...
if (process.env.VUE_APP_TEST === 'e2e') {
  // Ensure tests fail when Vue emits an error.
  Vue.config.errorHandler = window.Cypress.cy.onUncaughtException
}

const app = new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app')

// If running e2e tests...
if (process.env.VUE_APP_TEST === 'e2e') {
  // Attach the app to the window, which can be useful
  // for manually setting state in Cypress commands
  // such as `cy.logIn()`.
  window.__app__ = app
}
