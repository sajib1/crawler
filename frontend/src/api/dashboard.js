import axios from 'axios'
const ROOT_URL = process.env.VUE_APP_API_ROOT_URL
const HEADERS = { 'Content-Type': 'application/json' }

export default {
  fetchRequestedSites() {
    return axios.get(`${ROOT_URL}sites`)
  },
  addNewSite(Site) {
    return new Promise((resolve, reject) => {
      const url = `${ROOT_URL}sites`
      axios
        .post(url, JSON.stringify(Site), { headers: HEADERS })
        .then((response) => {
          resolve(response)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
}
