import axios from 'axios'
const ROOT_URL = process.env.VUE_APP_API_ROOT_URL
const HEADERS = { 'Content-Type': 'application/json' }

export default {
  fetchUsers() {
    return axios.get(`${ROOT_URL}users`)
  },
  addUser(User) {
    return new Promise((resolve, reject) => {
      const url = `${ROOT_URL}users`
      axios
        .post(url, JSON.stringify(User), { headers: HEADERS })
        .then((response) => {
          resolve(response)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  updateUser(User) {
    return new Promise((resolve, reject) => {
      const url = `${ROOT_URL}users/${User.id}`
      axios
        .put(url, User)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  deleteUser(id) {
    return new Promise((resolve, reject) => {
      const url = `${ROOT_URL}users/${id}`
      axios
        .delete(url, { headers: HEADERS })
        .then((response) => {
          resolve(response)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
}
