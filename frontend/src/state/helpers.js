import { mapState, mapGetters, mapActions } from 'vuex'

export const authComputed = {
  ...mapState('auth', {
    currentUser: (state) => state.currentUser,
  }),
  ...mapGetters('auth', ['loggedIn', 'currentUser']),
}

export const authMethods = mapActions('auth', ['logIn', 'logOut'])

export const userComputed = {
  ...mapGetters('users', ['users']),
}
export const userMethods = mapActions('users', [
  'fetchUsers',
  'addUser',
  'updateUser',
  'deleteUser',
  'setUsers',
])

export const siteComputed = {
  ...mapGetters('dashboards', ['requested_sites']),
}
export const siteMethods = mapActions('dashboards', [
  'fetchRequestedSites',
  'addNewSite',
])

export const customerComputed = {
  ...mapGetters('customers', ['customers']),
}
export const customerMethods = mapActions('customers', [
  'fetchCustomers',
  'addCustomer',
  'updateCustomer',
  'deleteCustomer',
  'setCustomers',
])
