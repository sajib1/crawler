import axios from 'axios'
import api from '@api/user'

export const state = {
  cached: [],
  users: [],
}

export const getters = {
  users: (state) => state.users,
}

export const mutations = {
  SET_USERS: (state, users) => {
    state.users = users
  },
  CACHE_USER(state, newUser) {
    state.cached.push(newUser)
  },
}

export const actions = {
  async fetchUsers({ commit, state, rootState }) {
    // const { token } = rootState.auth;
    const response = await api.fetchUsers()
    if (response.data.message === undefined) {
      commit('SET_USERS', response.data)
    }
  },
  addUser({ rootState }, user) {
    return api.addUser(user)
  },
  updateUser({ rootState }, user) {
    return api.updateUser(user)
  },
  deleteUser({ rootState }, data) {
    return api.deleteUser(data.id)
  },
  fetchUser({ commit, state, rootState }, { username }) {
    // 1. Check if we already have the user as a current user.
    const { currentUser } = rootState.auth
    if (currentUser && currentUser.username === username) {
      return Promise.resolve(currentUser)
    }

    // 2. Check if we've already fetched and cached the user.
    const matchedUser = state.cached.find((user) => user.username === username)
    if (matchedUser) {
      return Promise.resolve(matchedUser)
    }

    // 3. Fetch the user from the API and cache it in case
    //    we need it again in the future.
    return axios.get(`/users/${username}`).then((response) => {
      const user = response.data
      commit('CACHE_USER', user)
      return user
    })
  },
  setUsers({ commit }, users) {
    commit('SET_USERS', users)
  },
}
