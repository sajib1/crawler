import axios from 'axios'
import api from '@api/dashboard'

export const state = {
  cached: [],
  requested_sites: [],
  cancelled_sites: [],
}

export const getters = {
    requested_sites: (state) => state.requested_sites,
    cancelled_sites: (state) => state.cancelled_sites,
  }
  
  export const mutations = {
    SET_REQUESTED_SITES: (state, sites) => {
      state.requested_sites = sites
    },
    SET_CANCELLED_SITES: (state, sites) => {
      state.cancelled_sites = sites
    },
    ADD_SITE: (state, site) => {
      state.requested_sites = [...state.requested_sites,site]
    },
  }

  export const actions = {
    async fetchRequestedSites({ commit, state, rootState }) {
      const response = await api.fetchRequestedSites()
        commit('SET_REQUESTED_SITES', response.data)
    },
    async fetchCancelledSites({ commit, state, rootState }) {
      const response = await api.fetchCancelledSites()
      commit('SET_CANCELLED_SITES', response.data)
    },
    async addNewSite({commit},site){
      const response = await api.addNewSite(site)
      console.log('112',response.data)
      commit('ADD_SITE',response.data)
      return response.data;
    }
  }