import ActionBtnsVue from './action-btns-vue'

describe('@components/action-btns-vue', () => {
  it('exports a valid component', () => {
    expect(ActionBtnsVue).toBeAComponent()
  })
})
