import NavMenuGroup from './nav-menu-group'

describe('@components/nav-menu-group', () => {
  it('exports a valid component', () => {
    expect(NavMenuGroup).toBeAComponent()
  })
})
