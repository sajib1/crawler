import BaseButton from './_base-button'

describe('@components/_base-button', () => {
  it('exports a valid component', () => {
    expect(BaseButton).toBeAComponent()
  })
})
