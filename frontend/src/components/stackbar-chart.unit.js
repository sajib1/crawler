import StackbarChart from './stackbar-chart'

describe('@components/stackbar-chart', () => {
  it('exports a valid component', () => {
    expect(StackbarChart).toBeAComponent()
  })
})
