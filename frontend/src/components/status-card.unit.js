import StatusCard from './status-card'

describe('@components/status-card', () => {
  it('exports a valid component', () => {
    expect(StatusCard).toBeAComponent()
  })
})
