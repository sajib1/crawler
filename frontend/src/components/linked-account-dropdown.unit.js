import LinkedAccountDropdown from './linked-account-dropdown'

describe('@components/linked-account-dropdown', () => {
  it('exports a valid component', () => {
    expect(LinkedAccountDropdown).toBeAComponent()
  })
})
