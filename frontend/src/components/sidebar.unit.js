import Sidebar from './sidebar'

describe('@components/sidebar', () => {
  it('exports a valid component', () => {
    expect(Sidebar).toBeAComponent()
  })
})
