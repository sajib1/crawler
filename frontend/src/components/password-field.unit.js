import PasswordField from './password-field'

describe('@components/password-field', () => {
  it('exports a valid component', () => {
    expect(PasswordField).toBeAComponent()
  })
})
