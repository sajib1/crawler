import AccessField from './access-field'

describe('@components/access-field', () => {
  it('exports a valid component', () => {
    expect(AccessField).toBeAComponent()
  })
})
