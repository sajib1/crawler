import Footer from './footer'

describe('@components/footer', () => {
  it('exports a valid component', () => {
    expect(Footer).toBeAComponent()
  })
})
