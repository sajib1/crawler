import DoughnutChart from './doughnut-chart'

describe('@components/doughnut-chart', () => {
  it('exports a valid component', () => {
    expect(DoughnutChart).toBeAComponent()
  })
})
