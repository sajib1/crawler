import NavMenuItem from './nav-menu-item'

describe('@components/nav-menu-item', () => {
  it('exports a valid component', () => {
    expect(NavMenuItem).toBeAComponent()
  })
})
