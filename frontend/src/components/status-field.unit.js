import StatusField from './status-field'

describe('@components/status-field', () => {
  it('exports a valid component', () => {
    expect(StatusField).toBeAComponent()
  })
})
