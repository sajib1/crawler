import Submenu from './submenu'

describe('@components/submenu', () => {
  it('exports a valid component', () => {
    expect(Submenu).toBeAComponent()
  })
})
