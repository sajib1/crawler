import InvStatusField from './inv-status-field'

describe('@components/inv-status-field', () => {
  it('exports a valid component', () => {
    expect(InvStatusField).toBeAComponent()
  })
})
