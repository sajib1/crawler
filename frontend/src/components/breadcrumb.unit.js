import Breadcrumb from './breadcrumb'

describe('@components/breadcrumb', () => {
  it('exports a valid component', () => {
    expect(Breadcrumb).toBeAComponent()
  })
})
