const UserSerializer = {
  serialize({ id, username,password,email,type,account,date_created}) {
    return {
      id,
      username,
      password,
      email,
      type,
      account,
      date_created,
    };
  },
  serializeWithoutPassword({ id, username,email,type,account,date_created}) {
    return {
      id,
      username,
      email,
      type,
      account,
      date_created,
    };
  }
};

module.exports = UserSerializer;
