const { Router } = require('express');
const { inject } = require('awilix-express');
const Status = require('http-status');

const UsersController = {
  get router() {
    const router = Router();

    router.use(inject('userSerializer'));

    router.get('/', inject('getAllUsers','verifyToken'), this.index);
    router.get('/:id', inject('getUser','verifyToken'), this.show);
    router.post('/', inject('createUser','verifyToken'), this.create);
    router.put('/:id', inject('updateUser','verifyToken'), this.update);
    router.delete('/:id', inject('deleteUser','verifyToken'), this.delete);
    router.patch('/password/reset', inject('resetPassword','verifyToken'), this.resetPassword);

    return router;
  },

  index(req, res, next) {
    const { getAllUsers, userSerializer, verifyToken } = req;
    const { SUCCESS, ERROR } = getAllUsers.outputs;

    getAllUsers.on(SUCCESS, (users) => {
      res.status(Status.OK)
        .json(users.map(userSerializer.serialize));
    })
    .on(ERROR, next);

    verifyToken.execute(req,res);
    if (res.statusCode === 403) return;
    getAllUsers.execute();
  },

  show(req, res, next) {
    const { getUser, userSerializer, verifyToken } = req;
    const { SUCCESS, ERROR, NOT_FOUND } = getUser.outputs;

    getUser
      .on(SUCCESS, (user) => {
        res
          .status(Status.OK)
          .json(userSerializer.serialize(user));
      })
      .on(NOT_FOUND, (error) => {
        res.status(Status.NOT_FOUND).json({
          type: 'NotFoundError',
          details: error.details
        });
      })
      .on(ERROR, next);

    verifyToken.execute(req,res);
    if (res.statusCode === 403) return;
    getUser.execute(Number(req.params.id));
  },

  create(req, res, next) {
    const { createUser, userSerializer, verifyToken } = req;
    const { SUCCESS, ERROR, VALIDATION_ERROR } = createUser.outputs;

    createUser
      .on(SUCCESS, (user) => {
        res
          .status(Status.CREATED)
          .json(userSerializer.serializeWithoutPassword(user));
      })
      .on(VALIDATION_ERROR, (error) => {
        res.status(Status.BAD_REQUEST).json({
          type: 'ValidationError',
          details: error.details
        });
      })
      .on(ERROR, next);

    verifyToken.execute(req,res);
    if (res.statusCode === 403) return;
    createUser.execute(req.body);
  },

  update(req, res, next) {
    const { updateUser, userSerializer, verifyToken } = req;
    const { SUCCESS, ERROR, VALIDATION_ERROR, NOT_FOUND } = updateUser.outputs;

    updateUser
      .on(SUCCESS, (user) => {
        res
          .status(Status.ACCEPTED)
          .json(userSerializer.serializeWithoutPassword(user));
      })
      .on(VALIDATION_ERROR, (error) => {
        res.status(Status.BAD_REQUEST).json({
          type: 'ValidationError',
          details: error.details
        });
      })
      .on(NOT_FOUND, (error) => {
        res.status(Status.NOT_FOUND).json({
          type: 'NotFoundError',
          details: error.details
        });
      })
      .on(ERROR, next);

    verifyToken.execute(req,res);
    if (res.statusCode === 403) return;
    updateUser.execute(Number(req.params.id), req.body);
  },

  resetPassword(req, res, next) {
    const { resetPassword, verifyToken } = req;
    const { SUCCESS, ERROR, VALIDATION_ERROR, NOT_FOUND } = resetPassword.outputs;

    resetPassword
      .on(SUCCESS, () => {
        res
          .status(Status.ACCEPTED)
          .json({"message": req.body.user_id + " password changed successfully"});
      })
      .on(VALIDATION_ERROR, (error) => {
        res.status(Status.BAD_REQUEST).json({
          type: 'ValidationError',
          details: error.details
        });
      })
      .on(NOT_FOUND, (error) => {
        res.status(Status.NOT_FOUND).json({
          type: 'NotFoundError',
          details: error.details
        });
      })
      .on(ERROR, next);

    verifyToken.execute(req,res);
    if (res.statusCode === 403) return;
    resetPassword.execute(req.body);
  },

  delete(req, res, next) {
    const { deleteUser, verifyToken } = req;
    const { SUCCESS, ERROR,  NOT_FOUND } = deleteUser.outputs;

    deleteUser
      .on(SUCCESS, () => {
        res.status(Status.ACCEPTED).end();
      })
      .on(NOT_FOUND, (error) => {
        res.status(Status.NOT_FOUND).json({
          type: 'NotFoundError',
          details: error.details
        });
      })
      .on(ERROR, next);

    verifyToken.execute(req,res);
    deleteUser.execute(Number(req.params.id));
  }
};

module.exports = UsersController;
