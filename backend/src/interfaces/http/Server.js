const express = require('express');
const passport = require('passport');
const session = require('express-session');
const bodyParser = require('body-parser');
class Server {
  constructor({ config, router, logger }) {
    this.config = config;
    this.logger = logger;
    this.express = express();

    this.express.disable('x-powered-by');
    this.express.use(router);
    // this.express.use(passport.initialize());
    // this.express.use(session({ secret: 'keyboard cat',resave: true, saveUninitialized:true}));
    // this.express.use(passport.session());
  }

  start() {
    return new Promise((resolve) => {
      const http = this.express
        .listen(this.config.web.port, () => {
          const { port } = http.address();
          this.logger.info(`[p ${process.pid}] Listening at port ${port}`);
          resolve();
        });
    });
  }
}

module.exports = Server;
