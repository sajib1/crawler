const moment = require('moment');

module.exports = function formatDate(date) {
  return moment(date).format('MM/DD/YYYY');
};
