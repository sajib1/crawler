module.exports = function calculateOffset(page, size) {
    return (page - 1) * size;
}
