
const formatDate = require('../../http/utils/helper');

const SiteSerializer = {
  serialize({ id, company_name,job_url,due_date,instruction,created_date,is_active}) {
    return {
      id,
      company_name,
      job_url,
      due_date:formatDate(new Date(due_date)),
      instruction,
      created_date:formatDate(new Date(created_date)),
      is_active,
    };
  },
};

module.exports = SiteSerializer;
