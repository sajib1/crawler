const { Router } = require('express');
const { inject } = require('awilix-express');
const Status = require('http-status');

const SitesController = {
  get router() {
    const router = Router();

    router.use(inject('siteSerializer'));

    router.get('/', inject('getAllSites','verifyToken'), this.index);
    router.get('/:id', inject('getSite','verifyToken'), this.show);
    router.post('/', inject('createSite','verifyToken'), this.create);
    router.put('/:id', inject('updateSite','verifyToken'), this.update);
    router.delete('/:id', inject('deleteSite','verifyToken'), this.delete);

    return router;
  },

  index(req, res, next) {
    const { getAllSites, siteSerializer, verifyToken } = req;
    const { SUCCESS, ERROR } = getAllSites.outputs;

    getAllSites.on(SUCCESS, (sites) => {
      res.status(Status.OK)
        .json(sites.map(siteSerializer.serialize));
    })
    .on(ERROR, next);

    verifyToken.execute(req,res);
    if (res.statusCode === 403) return;
    getAllSites.execute();
  },

  show(req, res, next) {
    const { getSite, siteSerializer, verifyToken } = req;
    const { SUCCESS, ERROR, NOT_FOUND } = getSite.outputs;

    getSite
      .on(SUCCESS, (site) => {
        res
          .status(Status.OK)
          .json(siteSerializer.serialize(site));
      })
      .on(NOT_FOUND, (error) => {
        res.status(Status.NOT_FOUND).json({
          type: 'NotFoundError',
          details: error.details
        });
      })
      .on(ERROR, next);

    verifyToken.execute(req,res);
    if (res.statusCode === 403) return;
    getSite.execute(Number(req.params.id));
  },

  create(req, res, next) {
    const { createSite, siteSerializer, verifyToken } = req;
    const { SUCCESS, ERROR, VALIDATION_ERROR } = createSite.outputs;

    createSite
      .on(SUCCESS, (site) => {
        res
          .status(Status.CREATED)
          .json(siteSerializer.serialize(site));
      })
      .on(VALIDATION_ERROR, (error) => {
        res.status(Status.BAD_REQUEST).json({
          type: 'ValidationError',
          details: error.details
        });
      })
      .on(ERROR, next);

    verifyToken.execute(req,res);
    if (res.statusCode === 403) return;
    createSite.execute(req.body);
  },

  update(req, res, next) {
    const { updateSite, siteSerializer, verifyToken } = req;
    const { SUCCESS, ERROR, VALIDATION_ERROR, NOT_FOUND } = updateSite.outputs;

    updateSite
      .on(SUCCESS, (site) => {
        res
          .status(Status.ACCEPTED)
          .json(siteSerializer.serializeWithoutPassword(site));
      })
      .on(VALIDATION_ERROR, (error) => {
        res.status(Status.BAD_REQUEST).json({
          type: 'ValidationError',
          details: error.details
        });
      })
      .on(NOT_FOUND, (error) => {
        res.status(Status.NOT_FOUND).json({
          type: 'NotFoundError',
          details: error.details
        });
      })
      .on(ERROR, next);

    verifyToken.execute(req,res);
    if (res.statusCode === 403) return;
    updateSite.execute(Number(req.params.id), req.body);
  },

  delete(req, res, next) {
    const { deleteSite, verifyToken } = req;
    const { SUCCESS, ERROR,  NOT_FOUND } = deleteSite.outputs;

    deleteSite
      .on(SUCCESS, () => {
        res.status(Status.ACCEPTED).end();
      })
      .on(NOT_FOUND, (error) => {
        res.status(Status.NOT_FOUND).json({
          type: 'NotFoundError',
          details: error.details
        });
      })
      .on(ERROR, next);

    verifyToken.execute(req,res);
    deleteSite.execute(Number(req.params.id));
  }
};

module.exports = SitesController;
