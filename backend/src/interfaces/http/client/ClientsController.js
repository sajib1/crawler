const { Router } = require('express');
const { inject } = require('awilix-express');
const Status = require('http-status');

const ClientsController = {
  get router() {
    const router = Router();

    router.use(inject('clientSerializer'));

    router.get('/', inject('getAllClients','verifyToken'), this.index);
    router.get('/:id', inject('getClient','verifyToken'), this.show);
    router.post('/', inject('createClient','verifyToken'), this.create);
    router.put('/:id', inject('updateClient','verifyToken'), this.update);
    router.delete('/:id', inject('deleteClient','verifyToken'), this.delete);

    return router;
  },

  index(req, res, next) {
    const { getAllClients, clientSerializer, verifyToken } = req;
    const { SUCCESS, ERROR } = getAllClients.outputs;

    getAllClients
      .on(SUCCESS, (users) => {
        res
          .status(Status.OK)
          .json(users.map(clientSerializer.serialize));
      })
      .on(ERROR, next);

    verifyToken.execute(req,res);
    if (res.statusCode === 403) return;
    getAllClients.execute();
  },

  show(req, res, next) {
    const { getClient, clientSerializer, verifyToken } = req;

    const { SUCCESS, ERROR, NOT_FOUND } = getClient.outputs;

    getClient
      .on(SUCCESS, (client) => {
        res
          .status(Status.OK)
          .json(clientSerializer.serialize(client));
      })
      .on(NOT_FOUND, (error) => {
        res.status(Status.NOT_FOUND).json({
          type: 'NotFoundError',
          details: error.details
        });
      })
      .on(ERROR, next);

    verifyToken.execute(req,res);
    if (res.statusCode === 403) return;
    getClient.execute(Number(req.params.id));
  },

  create(req, res, next) {
    const { createClient, clientSerializer, verifyToken } = req;
    const { SUCCESS, ERROR, VALIDATION_ERROR } = createClient.outputs;

    createClient
      .on(SUCCESS, (user) => {
        res
          .status(Status.CREATED)
          .json(clientSerializer.serialize(user));
      })
      .on(VALIDATION_ERROR, (error) => {
        res.status(Status.BAD_REQUEST).json({
          type: 'ValidationError',
          details: error.details
        });
      })
      .on(ERROR, next);

    verifyToken.execute(req,res);
    if (res.statusCode === 403) return;
    createClient.execute(req.body);
  },

  update(req, res, next) {
    const { updateClient, clientSerializer, verifyToken } = req;
    const { SUCCESS, ERROR, VALIDATION_ERROR, NOT_FOUND } = updateClient.outputs;

    updateClient
      .on(SUCCESS, (user) => {
        res
          .status(Status.ACCEPTED)
          .json(clientSerializer.serialize(user));
      })
      .on(VALIDATION_ERROR, (error) => {
        res.status(Status.BAD_REQUEST).json({
          type: 'ValidationError',
          details: error.details
        });
      })
      .on(NOT_FOUND, (error) => {
        res.status(Status.NOT_FOUND).json({
          type: 'NotFoundError',
          details: error.details
        });
      })
      .on(ERROR, next);

    verifyToken.execute(req,res);
    if (res.statusCode === 403) return;
    updateClient.execute(Number(req.params.id), req.body);
  },

  delete(req, res, next) {
    const { deleteClient, verifyToken } = req;
    const { SUCCESS, ERROR,  NOT_FOUND } = deleteClient.outputs;

    deleteClient
      .on(SUCCESS, () => {
        res.status(Status.ACCEPTED).end();
      })
      .on(NOT_FOUND, (error) => {
        res.status(Status.NOT_FOUND).json({
          type: 'NotFoundError',
          details: error.details
        });
      })
      .on(ERROR, next);

    verifyToken.execute(req,res);
    if (res.statusCode === 403) return;
    deleteClient.execute(Number(req.params.id));
  }
};

module.exports = ClientsController;
