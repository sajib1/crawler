const ClientSerializer = {
  serialize({ id, account, name, date_created }) {
    return {
      id,
      account,
      name,
      date_created
    };
  }
};

module.exports = ClientSerializer;
