const { Router } = require('express');
const { inject } = require('awilix-express');
const Status = require('http-status');
const jwt = require('jsonwebtoken');

const AuthController = {
  get router() {
    const router = Router();
    router.use(inject('authSerializer'));
    router.post('/', inject('signIn'), this.signin);
    router.get('/', inject('verifyToken'), this.verifysignin);

    return router;
  },

  verifysignin(req, res, next) {
    const { verifyToken, authSerializer } = req
    verifyToken.execute(req, res)
    res.status(Status.OK).json(req.user);
  },

  signin(req, res, next) {
    const { signIn, authSerializer } = req;

    const { SUCCESS, ERROR, NOT_FOUND } = signIn.outputs;

    signIn
      .on(SUCCESS, (user) => {
        res
          .status(Status.OK)
          .json(authSerializer.serialize_get(user));
      })
      .on(NOT_FOUND, (error) => {
        res.status(Status.NOT_FOUND).json({
          type: 'NotFoundError',
          details: error.details
        });
      })
      .on(ERROR, next);

      signIn.execute(req.body);
  },
};

module.exports = AuthController;
