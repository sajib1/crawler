var jwt = require('jsonwebtoken');

const AuthSerializer = {
  serialize({email,password}) {
    return {
      email,
      password,
    };
  },
  serialize_get({id,username,email,type}) {
    const token = jwt.sign({ id,username,email,type }, process.env.ACCESS_TOKEN_SECRET, {
			expiresIn: 86400 // expires in 24 hours
		});
    return {
      id,
      username,
      email,
      auth:true,
      role:type,
      token
    };
  }
};

module.exports = AuthSerializer;
