const jwt = require('jsonwebtoken');
const Status = require('http-status');
class VerifyToken {
  constructor({authRepository }) {
    this.authRepository = authRepository;
  }

  async execute(reqData,res) {
    try {
      let token = reqData.headers['authorization'];
      if (token === undefined) {
        const error = new Error('NotTokenProvidedError');
        throw error;
      }
      reqData.token = token;
      jwt.verify(reqData.token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
        if (err) {
          return res.status(Status.UNAUTHORIZED).json({
            type: 'AuthorizationError',
            auth:false,
            details: 'Fail to Authentication. ' + err
          });
        }
        reqData.user = user
      });
    } catch(error) {
      res.status(Status.FORBIDDEN).json({
        type: 'NotTokenProvidedError',
        auth:false,
        details: 'Token not provided'
      });
    }
  }
}

module.exports = VerifyToken;
