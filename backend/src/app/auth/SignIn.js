const Operation = require('src/app/Operation');

class SignIn extends Operation {
  constructor({authRepository }) {
    super();
    this.authRepository = authRepository;
  }

  async execute(authData) {
    const { SUCCESS, ERROR, NOT_FOUND} = this.outputs;

    try {
      const authUser = await this.authRepository.getOne(authData);
      this.emit(SUCCESS, authUser);
    } catch(error) {
      this.emit(NOT_FOUND, {
        type: error.message,
        details: error.details
      });
    }
  }
}

SignIn.setOutputs(['SUCCESS', 'ERROR', 'NOT_FOUND']);

module.exports = SignIn;
