const Operation = require('src/app/Operation');
const _ = require("lodash");
const formatDate = require('../../interfaces/http/utils/helper');

class GetAllUsers extends Operation {
  constructor({ usersRepository }) {
    super();
    this.usersRepository = usersRepository;
  }

  async execute() {
    const { SUCCESS, ERROR } = this.outputs;

    try {
      let users = await this.usersRepository.getAll();

      users = _.forEach(users, user => {
        _.set(user, 'password', user.password !== "" ? `[set]` : ``);
        _.set(user, 'date_created', formatDate(new Date(user.date_created)));
      });

      this.emit(SUCCESS, users);
    } catch(error) {
      this.emit(ERROR, error);
    }
  }
}

GetAllUsers.setOutputs(['SUCCESS', 'ERROR']);

module.exports = GetAllUsers;
