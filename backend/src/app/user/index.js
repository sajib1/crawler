module.exports = {
  GetAllUsers: require('./GetAllUsers'),
  CreateUser: require('./CreateUser'),
  GetUser: require('./GetUser'),
  UpdateUser: require('./UpdateUser'),
  ResetPassword: require('./ResetPassword'),
  DeleteUser: require('./DeleteUser')
};
