const Operation = require('src/app/Operation');
const _ = require("lodash");
const formatDate = require('../../interfaces/http/utils/helper');

class GetClient extends Operation {
  constructor({ clientsRepository }) {
    super();
    this.clientsRepository = clientsRepository;
  }

  async execute(clientId) {
    const { SUCCESS, NOT_FOUND } = this.outputs;

    try {
      const client = await this.clientsRepository.getById(clientId);
      _.set(client, 'date_created', formatDate(new Date(client.date_created)));
      this.emit(SUCCESS, client);
    } catch(error) {
      this.emit(NOT_FOUND, {
        type: error.message,
        details: error.details
      });
    }
  }
}

GetClient.setOutputs(['SUCCESS', 'ERROR', 'NOT_FOUND']);

module.exports = GetClient;
