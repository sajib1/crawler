const Operation = require('src/app/Operation');
const Client = require('src/domain/client/Client');

class CreateClient extends Operation {
  constructor({ clientsRepository }) {
    super();
    this.clientsRepository = clientsRepository;
  }

  async execute(clientData) {
    const { SUCCESS, ERROR, VALIDATION_ERROR } = this.outputs;

    const client = new Client(clientData);

    try {
      const newClient = await this.clientsRepository.add(client);

      this.emit(SUCCESS, newClient);
    } catch(error) {
      if(error.message === 'ValidationError') {
        return this.emit(VALIDATION_ERROR, error);
      }

      this.emit(ERROR, error);
    }
  }
}

CreateClient.setOutputs(['SUCCESS', 'ERROR', 'VALIDATION_ERROR']);

module.exports = CreateClient;
