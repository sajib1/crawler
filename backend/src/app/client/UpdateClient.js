const Operation = require('src/app/Operation');

class UpdateClient extends Operation {
  constructor({ clientsRepository }) {
    super();
    this.clientsRepository = clientsRepository;
  }

  async execute(clientId, clientData) {
    const {
      SUCCESS, NOT_FOUND, VALIDATION_ERROR, ERROR
    } = this.outputs;

    try {
      const client = await this.clientsRepository.update(clientId, clientData);
      this.emit(SUCCESS, client);
    } catch(error) {
      switch(error.message) {
      case 'ValidationError':
        return this.emit(VALIDATION_ERROR, error);
      case 'NotFoundError':
        return this.emit(NOT_FOUND, error);
      default:
        this.emit(ERROR, error);
      }
    }
  }
}

UpdateClient.setOutputs(['SUCCESS', 'NOT_FOUND', 'VALIDATION_ERROR', 'ERROR']);

module.exports = UpdateClient;
