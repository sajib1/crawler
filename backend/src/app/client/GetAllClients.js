const Operation = require('src/app/Operation');
const _ = require("lodash");
const formatDate = require('../../interfaces/http/utils/helper');

class GetAllClients extends Operation {
  constructor({ clientsRepository }) {
    super();
    this.clientsRepository = clientsRepository;
  }

  async execute() {
    const { SUCCESS, ERROR } = this.outputs;

    try {
      let clients = await this.clientsRepository.getAll();

      clients = _.forEach(clients, client => {
        _.set(client, 'date_created', formatDate(new Date(client.date_created)));
      });

      this.emit(SUCCESS, clients);
    } catch(error) {
      this.emit(ERROR, error);
    }
  }
}

GetAllClients.setOutputs(['SUCCESS', 'ERROR']);

module.exports = GetAllClients;
