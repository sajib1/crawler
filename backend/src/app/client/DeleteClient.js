const Operation = require('src/app/Operation');

class DeleteClient extends Operation {
  constructor({ clientsRepository }) {
    super();
    this.clientsRepository = clientsRepository;
  }

  async execute(clientId) {
    const { SUCCESS, ERROR, NOT_FOUND } = this.outputs;

    try {
      await this.clientsRepository.remove(clientId);
      this.emit(SUCCESS);
    } catch(error) {
      if(error.message === 'NotFoundError') {
        return this.emit(NOT_FOUND, error);
      }

      this.emit(ERROR, error);
    }
  }
}

DeleteClient.setOutputs(['SUCCESS', 'ERROR', 'NOT_FOUND']);

module.exports = DeleteClient;
