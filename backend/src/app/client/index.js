module.exports = {
  GetAllClients: require('./GetAllClients'),
  CreateClient: require('./CreateClient'),
  GetClient: require('./GetClient'),
  UpdateClient: require('./UpdateClient'),
  DeleteClient: require('./DeleteClient')
};
