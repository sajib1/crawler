module.exports = {
  GetAllSites: require('./GetAllSites'),
  CreateSite: require('./CreateSite'),
  GetSite: require('./GetSite'),
  UpdateSite: require('./UpdateSite'),
  DeleteSite: require('./DeleteSite')
};
