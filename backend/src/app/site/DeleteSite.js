const Operation = require('src/app/Operation');

class DeleteSite extends Operation {
  constructor({ sitesRepository }) {
    super();
    this.sitesRepository = sitesRepository;
  }

  async execute(siteId) {
    const { SUCCESS, ERROR, NOT_FOUND } = this.outputs;

    try {
      await this.sitesRepository.remove(siteId);
      this.emit(SUCCESS);
    } catch(error) {
      if(error.message === 'NotFoundError') {
        return this.emit(NOT_FOUND, error);
      }

      this.emit(ERROR, error);
    }
  }
}

DeleteSite.setOutputs(['SUCCESS', 'ERROR', 'NOT_FOUND']);

module.exports = DeleteSite;
