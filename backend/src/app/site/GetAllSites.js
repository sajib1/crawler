const Operation = require('src/app/Operation');
const _ = require("lodash");
const formatDate = require('../../interfaces/http/utils/helper');

class GetAllSites extends Operation {
  constructor({ sitesRepository }) {
    super();
    this.sitesRepository = sitesRepository;
  }

  async execute() {
    const { SUCCESS, ERROR } = this.outputs;

    try {
      let sites = await this.sitesRepository.getAll();
      this.emit(SUCCESS, sites);
    } catch(error) {
      this.emit(ERROR, error);
    }
  }
}

GetAllSites.setOutputs(['SUCCESS', 'ERROR']);

module.exports = GetAllSites;
