const Operation = require('src/app/Operation');
const _ = require("lodash");
const formatDate = require('../../interfaces/http/utils/helper');

class GetSite extends Operation {
  constructor({ sitesRepository }) {
    super();
    this.sitesRepository = sitesRepository;
  }

  async execute(siteId) {
    const { SUCCESS, NOT_FOUND } = this.outputs;

    try {
      const site = await this.sitesRepository.getById(siteId);
      _.set(site, 'date_created', formatDate(new Date(site.date_created)));
      this.emit(SUCCESS, site);
    } catch(error) {
      this.emit(NOT_FOUND, {
        type: error.message,
        details: error.details
      });
    }
  }
}

GetSite.setOutputs(['SUCCESS', 'ERROR', 'NOT_FOUND']);

module.exports = GetSite;
