const Operation = require('src/app/Operation');
const Site = require('src/domain/site/Site');

class CreateSite extends Operation {
  constructor({ sitesRepository }) {
    super();
    this.sitesRepository = sitesRepository;
  }

  async execute(siteData) {
    const { SUCCESS, ERROR, VALIDATION_ERROR } = this.outputs;

    const site = new Site(siteData);

    try {
      const newSite = await this.sitesRepository.add(site);

      this.emit(SUCCESS, newSite);
    } catch(error) {
      if(error.message === 'ValidationError') {
        return this.emit(VALIDATION_ERROR, error);
      }

      this.emit(ERROR, error);
    }
  }
}

CreateSite.setOutputs(['SUCCESS', 'ERROR', 'VALIDATION_ERROR']);

module.exports = CreateSite;
