const { createContainer, asClass, asFunction, asValue } = require('awilix');
const { scopePerRequest } = require('awilix-express');

const config = require('../config');
const Application = require('./app/Application');
const {
  CreateUser,
  GetAllUsers,
  GetUser,
  UpdateUser,
  ResetPassword,
  DeleteUser
} = require('./app/user');

const {
  CreateSite,
  GetAllSites,
} = require('./app/site');

const {
  SignIn,
  VerifyToken,
} = require('./app/auth');


const {
  CreateClient,
  GetAllClients,
  GetClient,
  UpdateClient,
  DeleteClient
} = require('./app/client');



const UserSerializer = require('./interfaces/http/user/UserSerializer');
const SiteSerializer = require('./interfaces/http/site/SiteSerializer');
const ClientSerializer = require('./interfaces/http/client/ClientSerializer');
const AuthSerializer = require('./interfaces/http/auth/AuthSerializer');

const Server = require('./interfaces/http/Server');
const router = require('./interfaces/http/router');
const loggerMiddleware = require('./interfaces/http/logging/loggerMiddleware');
const errorHandler = require('./interfaces/http/errors/errorHandler');
const devErrorHandler = require('./interfaces/http/errors/devErrorHandler');
const swaggerMiddleware = require('./interfaces/http/swagger/swaggerMiddleware');

const logger = require('./infra/logging/logger');
const SequelizeUsersRepository = require('./infra/user/SequelizeUsersRepository');
const SequelizeSitesRepository = require('./infra/site/SequelizeSitesRepository');
const SequelizeAuthRepository = require('./infra/auth/SequelizeAuthRepository');
const SequelizeClientsRepository = require('./infra/client/SequelizeClientsRepository');

const {
  database,
  User: UserModel,
  Site: SiteModel,
  Client: ClientModel,
} = require('./infra/database/models');

const container = createContainer();

// System
container
  .register({
    app: asClass(Application).singleton(),
    server: asClass(Server).singleton()
  })
  .register({
    router: asFunction(router).singleton(),
    logger: asFunction(logger).singleton()
  })
  .register({
    config: asValue(config)
  });

// Middlewares
container
  .register({
    loggerMiddleware: asFunction(loggerMiddleware).singleton()
  })
  .register({
    containerMiddleware: asValue(scopePerRequest(container)),
    errorHandler: asValue(config.production ? errorHandler : devErrorHandler),
    swaggerMiddleware: asValue([swaggerMiddleware])
  });

// Repositories
container.register({
  usersRepository: asClass(SequelizeUsersRepository).singleton(),
  sitesRepository: asClass(SequelizeSitesRepository).singleton(),
  authRepository: asClass(SequelizeAuthRepository).singleton(),
  clientsRepository: asClass(SequelizeClientsRepository).singleton(),
});

// Database
container.register({
  database: asValue(database),
  UserModel: asValue(UserModel),
  SiteModel: asValue(SiteModel),
  ClientModel: asValue(ClientModel),
});

// Operations
container.register({
  createUser: asClass(CreateUser),
  getAllUsers: asClass(GetAllUsers),
  getUser: asClass(GetUser),
  updateUser: asClass(UpdateUser),
  resetPassword: asClass(ResetPassword),
  deleteUser: asClass(DeleteUser),

  createClient: asClass(CreateClient),
  getAllClients: asClass(GetAllClients),
  getClient: asClass(GetClient),
  updateClient: asClass(UpdateClient),
  deleteClient: asClass(DeleteClient),

  getAllSites: asClass(GetAllSites),
  createSite: asClass(CreateSite),

  signIn: asClass(SignIn),
  verifyToken:asClass(VerifyToken),

});

// Serializers
container.register({
  userSerializer: asValue(UserSerializer),
  siteSerializer: asValue(SiteSerializer),
  authSerializer: asValue(AuthSerializer),
  
  clientSerializer: asValue(ClientSerializer),
  
});

module.exports = container;
