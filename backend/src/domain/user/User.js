const { attributes } = require('structure');

const User = attributes({
  id: Number,
  username: {
    type: String,
    required: true
  },
  password:String,
  email:String,
  type:String,
  date_created:{
    type: String,
    required: true
  },
})(class User {
  isLegal() {
    return true;
  }
});


module.exports = User;
