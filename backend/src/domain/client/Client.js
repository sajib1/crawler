const { attributes } = require('structure');

const Client = attributes({
  id: Number,
  account:String,
  name: {
    type: String,
    required: true
  },
  date_created:{
    type: String,
    required: true
  },
})(class Clients {
  isLegal() {
    return true;
  }
});


module.exports = Client;
