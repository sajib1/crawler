const { attributes } = require('structure');
const Site = attributes({
  id: Number,
  company_name: {
    type: String,
    required: true
  },
  job_url:String,
  due_date:{
    type: String,
    required: true
  },
  instruction:String,
  created_date:{
    type: String,
  },
  is_active:Number,
})(class Site {
  isLegal() {
    return true;
  }
});


module.exports = Site;
