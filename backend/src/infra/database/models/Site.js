'use strict';
module.exports = function(sequelize, DataTypes) {
  const Site = sequelize.define('site', {
    company_name: DataTypes.STRING,
    job_url: DataTypes.STRING,
    due_date: DataTypes.DATE,
    instruction: DataTypes.STRING,
    created_date: {
      type: 'TIMESTAMP',
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      allowNull: false
    },
    is_active: DataTypes.INTEGER,
  }, {
    timestamps: false,
    classMethods: {
      associate() {
        // associations can be defined here
      }
    }
  });

  return Site;
};
