'use strict';

module.exports = function(sequelize, DataTypes) {
  const Client = sequelize.define('client', {
    account: DataTypes.STRING,
    name: DataTypes.STRING,
    date_created: DataTypes.DATE,
  }, {
    timestamps: false,
    classMethods: {
      associate() {
        // associations can be defined here
      }
    }
  });

  return Client;
};
