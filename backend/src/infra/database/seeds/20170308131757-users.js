  
'use strict';

const dataFaker = require('src/infra/support/dataFaker');

module.exports = {
  up: function (queryInterface) {
    const users = [];

    for(let i = 0; i < 1; i++) {
        users.push(
        {
            username:'SIEMENS',
            password:'$2y$10$Yd1bQnW098b7.fZy1j4tp.4wKJzwCm83HcSoIw5sFFJS/7WmERyuG', 
            email:'', 
            type:'client', 
            account:'SIEMENS',
            createdAt:'2019-12-19 17:57:36',
            updatedAt:new Date()
        },
        {
            username:'FLUOR-BRUNSWICK',
            password:'$2y$10$3T8NcvwXk6ACfGRNDnaeeeE.P.MKF8SSxWa604ZgM4VvkQsvZf8FO', 
            email:'', 
            type:'client', 
            account:'FLUOR BRUNSWICK CITY', 
            createdAt:'2019-12-19 17:57:36',
            updatedAt:new Date()
        },
        {
            username:'EAS-PG',
            password:'$2y$10$O2LwCFNP66LURuHRxtcHguGFiMpeDBADBF1PVpG/77aNeBn0wm7ym', 
            email:'', 
            type:'client', 
            account:'',
            createdAt:'2020-05-21 18:55:06',
            updatedAt:new Date()
        },
        {
            username:'FLUOR-UR',
            password:'$2y$10$DVkgyMWOdkpfoIh487uCduBjWe8fERkYrZpJP9dEmeK7r9HVYw7fu', 
            email:'', 
            type:'client', 
            account:'FLUOR-UR', 
            createdAt:'2019-12-19 17:57:36',
            updatedAt:new Date()
        },
        {
            username:'AMEC-GP',
            password:'$2y$10$1mb1pC7f/df3gwmWdv0ZwuQaLbZdzLKGI5J3daEIcT05.HI9vlW2i', 
            email:'', 
            type:'client', 
            account:'AMEC-GP', 
            createdAt:'2019-12-19 17:57:36',
            updatedAt:new Date()
        },
        {
            username:'NEMS',
            password:'$2y$10$LEMRv9WGRzXNtfeRIwmAPubkEm39hQjQgybkg1QcJ1LuUGVafJ4RO', 
            email:'', 
            type:'client', 
            account:'NEMS', 
            createdAt:'2019-12-19 17:57:36',
            updatedAt:new Date()
        },
        {
            username:'OCEANEERING',
            password:'$2y$10$zFVOl/IqEod/P01m.v/OZu.q9.kmuzqzmr3EVdij25kLHLPfR928y', 
            email:'', 
            type:'client', 
            account:'OCEANEERING', 
            createdAt:'2019-12-19 17:57:36',
            updatedAt:new Date()
        },
        {
            username:'FLUOR-GREENSVILLE',
            password:'$2y$10$bEgUMxU7maXDU7OQ.PuH/umTStSB3rAkcn1GbdY73Wv6mPZidxDce', 
            email:'', 
            type:'client', 
            account:'FLUOR-GREENSVILLE', 
            createdAt:'2019-12-19 17:57:36',
            updatedAt:new Date()
        },
        {
            username:'CIANBRO',
            password:'$2y$10$X45wEwD/Fbn7REPS/KZbDOrts8d1HxFKZN8ZL0AMX7e.ywta6vsaG', 
            email:'', 
            type:'client', 
            account:'CIANBRO', 
            createdAt:'2019-12-19 17:57:36',
            updatedAt:new Date()
        },
        {
            username: 'CRODA',
            password:'$2y$10$TEzkSq9QVE1Mdc8AOAYKsO1kEhhoJyvigLyx8o4IAxWHKwp8uv7lO', 
            email:'', 
            type:'client', 
            account:'CRODA', 
            createdAt:'2019-12-19 17:57:36',
            updatedAt:new Date()
        },
        {
            username: 'ORTEC',
            password:'$2y$10$KVwhDSW7LNbQLd0GgOthxe6xSkakhsroHG1hQZINgCe0zSomUN.Sm', 
            email:'', 
            type:'client', 
            account:'ORTEC', 
            createdAt:'2019-12-19 17:57:36',
            updatedAt:new Date()
        },
        {
            username: 'AMECFW',
            password:'$2y$10$I4XVkAYZ7MouevQKT1t3uOJr6vdDAg3NrUNPfNGmS8j74Sjz171jS', 
            email:'', 
            type:'client', 
            account:'AMECFW', 
            createdAt:'2019-12-19 17:57:36',
            updatedAt:new Date()
        },
        {
            username: 'CASEY',
            password:'$2y$10$de/9D9yw5lssKTGmIj0kR.X0fS4Doo1ZrWyo1AG7ySyVDKCNKjule', 
            email:'', 
            type:'client', 
            account:'CASEY', 
            createdAt:'2019-12-19 17:57:36',
            updatedAt:new Date()
        },
        {
            username: 'AUSTIN',
            password:'$2y$10$V8txGd2TO6gp5mb9sS2sceKouhKDOOaNRLO4BJ/lqcAmIW.tNOgMy', 
            email:'', 
            type:'client', 
            account:'AUSTIN', 
            createdAt:'2019-12-19 17:57:36',
            updatedAt:new Date()
        },
        {
            username: 'SUMMIT',
            password:'$2y$10$Fquv/BJWwrKzHJ3R2DaWcOwGUTUpxYcU4HMW88TEAlJlgyarQGgdO', 
            email:'', 
            type:'client', 
            account:'SUMMIT', 
            createdAt:'2019-12-19 17:57:36',
            updatedAt:new Date()
        },
        {
            username: 'CELANESE',
            password:'$2y$10$d8rYtNbkQwC3x4q.5Oifc.LSh9NbH69hjo9uJyM2b86gIOM4TJsa.', 
            email:'', 
            type:'client', 
            account:'CELANESE', 
            createdAt:'2019-12-19 17:57:36',
            updatedAt:new Date()
        },
        {
            username: 'SOLVAY',
            password:'$2y$10$bkZd90rySCFNPa0aqRQJmu6SniGP.gjLA3WCuoD8Y/1rW/ZuhHTXK', 
            email:'', 
            type:'client', 
            account:'SOLVAY', 
            createdAt:'2019-12-19 17:57:36',
            updatedAt:new Date()
        },
        {
            username: 'WOOD PLC',
            password:'$2y$10$AFlzYM0X2b/1S98gj3y6nOmus3OAPxG9RWR2XCBHIjcmeGJHu.Ltm', 
            email:'', 
            type:'client', 
            account:'WOOD PLC', 
            createdAt:'2019-12-19 17:57:36',
            updatedAt:new Date()
        },
        {
            username: 'FLUOR-NN',
            password:'$2y$10$Eh3wmPuL6yR/Xltn25tX.er/iz3KdJE/fiwRNXhhIiAL/txUrmIji', 
            email:'', 
            type:'client', 
            account:'FLUOR-NN', 
            createdAt:'2019-12-19 17:57:36',
            updatedAt:new Date()
        },
        {
            username: 'mservidio',
            password:'$2y$10$H7Xq8O75fykLUfBgWGT4yekHbN5bKHrjyNrL2eNN3PUfmDhbTi2nC', 
            email:'michael@michaelservidio.com', 
            type:'super_admin', 
            account:'FLUOR-GREENSVILLE', 
            createdAt:'2020-05-22 11:12:04',
            updatedAt:new Date()
        },
        {
            username: 'test_user',
            password:'$2y$10$2jW4nTrTzFh1zl.YqqoAIut0iLNa.erfAeSYz1diZDMO.Emr71Grm', 
            email:'test@test.com', 
            type:'super_admin', 
            account:'AA', 
            createdAt:'2020-05-21 19:36:35',
            updatedAt:new Date()
        },
        {
            username: 'jsmith',
            password:'$2y$10$ZdDY7eBC5DhGFlrMs3f7e.A4WE2mrvTDBW7l.P7tz.CThfo/N01IW', 
            email:'jsmith@usindustrialpiping.com', 
            type:'employee', 
            account:'AA', 
            createdAt:'2020-05-25 11:17:14',
            updatedAt:new Date()
        },
        {
            username: 'jdoe',
            password:'$2y$10$IIjl/epw5P7gzb24IJV0y.i1zp3CDQjXVN3FoPOilp5.ys9GGmZMK', 
            email:'jdoe@usindustrialpiping.com', 
            type:'employee', 
            account:'FHG', 
            createdAt:'2020-05-25 11:18:43',
            updatedAt:new Date()
        },
      );
    }

    return queryInterface.bulkInsert('users', users, {});
  },

  down: function (queryInterface) {
    return queryInterface.bulkDelete('users', null, {});
  }
};