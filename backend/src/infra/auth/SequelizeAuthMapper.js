const User = require('src/domain/user/User');

const SequelizeAuthMapper = {
  toEntity({ dataValues }) {
    const {id, username, password, email, type, account, date_created} = dataValues;
    return new User({ id, username, password, email, type, account, date_created  });
  },

  toDatabase(survivor) {
    const { username, password, email, type, account, date_created} = survivor;
    return { username, password, email, type, account, date_created };
  }
};

module.exports = SequelizeAuthMapper;
