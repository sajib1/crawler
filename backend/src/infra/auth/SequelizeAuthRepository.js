const AuthMapper = require('./SequelizeAuthMapper');
const jwt = require('jsonwebtoken');
const passport = require('passport')
var bcrypt = require('bcryptjs');
class SequelizeAuthRepository {
  constructor({ UserModel }) {
    this.UserModel = UserModel;
  }
  isValidPassword(userpass, password) {
    return bcrypt.compareSync(userpass, password);
  }


  async getOne(user) {
    const authUser = await this._getOne(user);
    
    return AuthMapper.toEntity(authUser);
  }

  async count() {
    return await this.UserModel.count();
  }

  
  // Private

  async _getOne(user) {
    try {
      let authuser = await this.UserModel.findOne({ where: {email: user.email } });
      let is_password_match = await this.isValidPassword(user.password, authuser.password);
      if(!is_password_match){
        const notFoundError = new Error('AuthorizationError');
        throw notFoundError;
      }
      return authuser;
    } catch(error) {
      if(error.name === 'SequelizeEmptyResultError') {
        const notFoundError = new Error('NotFoundError');
        notFoundError.details = `User with name ${user.username} can't be found.`;

        throw notFoundError;
      }

      throw error;
    }
  }
}

module.exports = SequelizeAuthRepository;
