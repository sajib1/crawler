const Site = require('src/domain/site/Site');

const SequelizeSiteMapper = {
  toEntity({ dataValues }) {
    const { id, company_name,job_url,due_date,instruction,created_date,is_active} = dataValues;
    return new Site({ id, company_name,job_url,due_date,instruction,created_date,is_active});
  },

  toDatabase(survivor) {
    const { company_name,job_url,due_date,instruction,created_date,is_active } = survivor;
    return { company_name,job_url,due_date,instruction,created_date,is_active};
  },

  toDatabaseForAdd(survivor) {
    const { company_name,job_url,due_date,instruction } = survivor;
    return { company_name,job_url,due_date,instruction};
  }
};

module.exports = SequelizeSiteMapper;
