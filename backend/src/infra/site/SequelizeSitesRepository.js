const SequelizeSiteMapper = require('./SequelizeSiteMapper');
const bcrypt = require("bcrypt");

class SequelizeSitesRepository {
  constructor({SiteModel }) {
    this.SiteModel = SiteModel;
  }

  async getAll(...args) {
    const sites = await this.SiteModel.findAll(...args);
    return sites.map(SequelizeSiteMapper.toEntity);
  }

  async getById(id) {
    const site = await this._getById(id);

    return SequelizeSiteMapper.toEntity(site);
  }

  async add(site) {
    const { valid, errors } = site.validate();

    if(!valid) {
      const error = new Error('ValidationError');
      error.details = errors;

      throw error;
    }

    const newSite = await this.SiteModel.create(SequelizeSiteMapper.toDatabaseForAdd(site));
    return SequelizeSiteMapper.toEntity(newSite);
  }

  async remove(id) {
    const site = await this._getById(id);

    await site.destroy();
    return;
  }

  async update(id, newData) {
    const site = await this._getById(id);

    const transaction = await this.SiteModel.sequelize.transaction();

    try {
      const updatedSite = await site.update(newData, { transaction });
      const siteEntity =SequelizeSiteMapper.toEntity(updatedSite);

      const { valid, errors } = siteEntity.validate();

      if(!valid) {
        const error = new Error('ValidationError');
        error.details = errors;

        throw error;
      }

      await transaction.commit();

      return siteEntity;
    } catch(error) {
      await transaction.rollback();

      throw error;
    }
  }

  async count() {
    return await this.SiteModel.count();
  }

  // Private

  async _getById(id) {
    try {
      return await this.SiteModel.findById(id, { rejectOnEmpty: true });
    } catch(error) {
      if(error.name === 'SequelizeEmptyResultError') {
        const notFoundError = new Error('NotFoundError');
        notFoundError.details = `Site with id ${id} can't be found.`;

        throw notFoundError;
      }

      throw error;
    }
  }
}

module.exports = SequelizeSitesRepository;
