const ClientMapper = require('./SequelizeClientMapper');

class SequelizeClientsRepository {
  constructor({ ClientModel }) {
    this.ClientModel = ClientModel;
  }

  async getAll(...args) {
    const clients = await this.ClientModel.findAll(...args);

    return clients.map(ClientMapper.toEntity);
  }

  async getById(id) {
    const client = await this._getById(id);

    return ClientMapper.toEntity(client);
  }

  async add(client) {
    const { valid, errors } = client.validate();

    if(!valid) {
      const error = new Error('ValidationError');
      error.details = errors;

      throw error;
    }

    const newClient = await this.ClientModel.create(ClientMapper.toDatabase(client));
    return ClientMapper.toEntity(newClient);
  }

  async remove(id) {
    const client = await this._getById(id);

    await client.destroy();
    return;
  }

  async update(id, newData) {
    const client = await this._getById(id);

    const transaction = await this.ClientModel.sequelize.transaction();

    try {
      const updatedClient = await client.update(newData, { transaction });
      const clientEntity = ClientMapper.toEntity(updatedClient);

      const { valid, errors } = clientEntity.validate();

      if(!valid) {
        const error = new Error('ValidationError');
        error.details = errors;

        throw error;
      }

      await transaction.commit();

      return clientEntity;
    } catch(error) {
      await transaction.rollback();

      throw error;
    }
  }

  async count() {
    return await this.ClientModel.count();
  }

  // Private

  async _getById(id) {
    try {
      return await this.ClientModel.findById(id, { rejectOnEmpty: true });
    } catch(error) {
      if(error.name === 'SequelizeEmptyResultError') {
        const notFoundError = new Error('NotFoundError');
        notFoundError.details = `Client with id ${id} can't be found.`;

        throw notFoundError;
      }

      throw error;
    }
  }
}

module.exports = SequelizeClientsRepository;
