const Client = require('src/domain/client/Client');

const SequelizeClientMapper = {
  toEntity({ dataValues }) {
    const { id, account, name, date_created } = dataValues;

    return new Client({ id, account, name, date_created });
  },

  toDatabase(survivor) {
    const { account, name, date_created } = survivor;

    return { account, name, date_created };
  }
};

module.exports = SequelizeClientMapper;
