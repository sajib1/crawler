const User = require('src/domain/user/User');

const SequelizeUserMapper = {
  toEntity({ dataValues }) {
    const { id, username, password, email, type, date_created } = dataValues;

    return new User({ id, username, password, email, type, date_created });
  },

  toDatabase(survivor) {
    const { username, password, email, type, date_created } = survivor;

    return { username, password, email, type, date_created };
  }
};

module.exports = SequelizeUserMapper;
